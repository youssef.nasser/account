<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <title>Records</title>
</head>
<body>
    <h1>Records Index</h1>

    <h2>Accounts</h2>
    <ul>
        @foreach($accounts as $account)
            <li>{{ $account->name }}</li>
        @endforeach
    </ul>

    <h2>Projects</h2>
    <ul>
        @foreach($projects as $project)
            <li>{{ $project->name }} ({{ $project->price }})</li>
        @endforeach
    </ul>

    <h2>Jobs</h2>
    <ul>
        @foreach($jobs as $job)
            <li>{{ $job->name }} ({{ $job->amount }})</li>
        @endforeach
    </ul>

    <h2>Tasks</h2>
    <ul>
        @foreach($tasks as $task)
            <li>{{ $task->name }}</li>
        @endforeach
    </ul>
</body>
</html>
