<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <title>Low Price Projects</title>
</head>
<body>
    <h1>Tasks with Low Price Projects</h1>

    <ul>
        @foreach($tasksWithLowPriceProject as $task)
            <li>{{ $task->name }} ({{ $task->job->project->price }})</li>
        @endforeach
    </ul>
</body>
</html>
