# Task

Create table “accounts” (id – name). “accounts” has many “projects” (id – name – price-
account_id) “projects” has many “jobs” (id – name – amount – project_id). “jobs” has many

“tasks” (id-name-job_id).

# Solution

1. **Accounts Table:**
   - Columns: id, name.

2. **Projects Table:**
   - Columns: id, name, price, account_id (Foreign Key).

3. **Jobs Table:**
   - Columns: id, name, amount, project_id (Foreign Key).

4. **Tasks Table:**
   - Columns: id, name, job_id (Foreign Key).

5. **Account Model:**
   - Relationships: One-to-Many with projects.

6. **Project Model:**
   - Relationships: Many-to-One with accounts, One-to-Many with jobs.

7. **Job Model:**
   - Relationships: Many-to-One with projects, One-to-Many with tasks.

8. **Task Model:**
   - Relationships: Many-to-One with jobs.
