<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Insert
Route::get('/insert-records', [ProjectController::class, 'insertRecords']);

// Select
Route::get('/select-records', [ProjectController::class, 'selectRecords']);

// Select tasks that belongs to project with price < 100
Route::get('/select-tasks-low-price', [ProjectController::class, 'selectTasksWithLowPrice']);
