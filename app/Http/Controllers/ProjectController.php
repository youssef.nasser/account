<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Task;
use App\Models\Account;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Insert sample records into the database for testing.
     *
     * Creates a sample account, project, job, and task. Returns success message.
     *
     * @return string
     */
    public function insertRecords()
    {
        $account = Account::create(['name' => 'Sample Account']);

        $project = $account->projects()->create(['name' => 'Sample Project', 'price' => 50.00]);

        $job = $project->jobs()->create(['name' => 'Sample Job', 'amount' => 25.00]);

        $task = $job->tasks()->create(['name' => 'Sample Task']);

        return "Records inserted successfully!";
    }

    /**
     * Insert sample records for testing.
     *
     * Creates a sample account, project, job, and task.
     *
     * @return string
     */
    public function selectRecords()
    {
        $accounts = Account::all();

        $projects = Project::with('account')->get();

        $jobs = Job::with('project')->get();

        $tasks = Task::with('job')->get();

        return view('records.index', compact('accounts', 'projects', 'jobs', 'tasks'));
    }

    /**
     * Select tasks from projects with a price below 100.
     *
     * Uses Eloquent's whereHas for nested relationship querying.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function selectTasksWithLowPrice()
    {
        $tasksWithLowPriceProject = Task::whereHas('job.project', function ($query) {
            $query->where('price', '<', 100);
        })->get();

        return view('tasks.low_price', compact('tasksWithLowPriceProject'));
    }
}
