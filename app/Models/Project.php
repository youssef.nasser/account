<?php

namespace App\Models;

use App\Models\Job;
use App\Models\Account;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory;

    /**
     * Fillable attributes for the model.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
    ];

    /**
     * Get the account associated with this project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account() : BelongsTo
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * Get the jobs associated with this project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs() : HasMany
    {
        return $this->hasMany(Job::class);
    }
}
