<?php

namespace App\Models;

use App\Models\Task;
use App\Models\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Job extends Model
{
    use HasFactory;

    /**
     * Fillable attributes for the model.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'amount',
    ];

    /**
     * Get the project associated with this task.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project() : BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the tasks associated with this job.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks() : HasMany
    {
        return $this->hasMany(Task::class);
    }
}
